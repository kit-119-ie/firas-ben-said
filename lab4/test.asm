NULL              EQU 0                         ; Constants
STD_OUTPUT_HANDLE EQU -11

extern _GetStdHandle@4                          ; Import external symbols
extern _WriteFile@20                           ; Windows API functions, decorated
extern _ExitProcess@4
extern wsprintfA
extern printf
global Start                                    ; Export symbols. The entry point

section .data                                   ; Initialized data segment
 Message        db "the price after discount is  ",0
 MessageLength  EQU $-Message                   ; Address of this line ($) - address of Message
 i dq 6
 b dq 8
 c dq 11
 d dq 3

section .bss                                    ; Uninitialized data segment
 StandardHandle resd 1
 Written        resd 1
 res resd 32
section .text                                   ; Code segment
Start:
 mov ax, [i]
 mov bx, [b]
 mov cx, [c]
 mov dl, [d]
 
add ax, bx
add ax, cx
div dl
add sp , ax 
 mov ax, [i]
 mov bx, [b]
 mov cx, [c]
 add ax, bx
add ax, cx
sub ax , sp

add ax, '0'
 
 ; adding ascii '0' to convert it into a string
 
 mov [res], ax


 push  STD_OUTPUT_HANDLE
 call  _GetStdHandle@4
 mov   dword [StandardHandle], EAX

	push  NULL                                     ; 5th parameter
 push  Written                                  ; 4th parameter
 push  MessageLength                            ; 3rd parameter
 push  Message                                  ; 2nd parameter
 push  dword [StandardHandle]                   ; 1st parameter
 call  _WriteFile@20                            ; Output can be redirect to a file using >

 push  NULL                                     ; 5th parameter
 push  Written                                  ; 4th parameter
 push  1                            ; 3rd parameter
 push  res                                  ; 2nd parameter
 push  dword [StandardHandle]                   ; 1st parameter
 call  _WriteFile@20                            ; Output can be redirect to a file using >

 push  NULL
 call  _ExitProcess@4
