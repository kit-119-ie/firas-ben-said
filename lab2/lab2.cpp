#include <iostream>
int main() {

	char i, b, dres, mres;


	__asm {
	//	mov ax, 0

		mov i, 10
		mov b, 3

	/*	mov al, i
		mov bl, b

		div bl

		mov dres, al*/


      
		mov al, i
		cbw         //(convert byte to word)
		mov bl, b

		div bl
		mov mres, al     //mres = i/b

		mov al, i
		mov bl, b
		mul bl
		mov dres, al         //dres = i*b
		mov bl, mres


		
		push al      //swap values
		pop cl //dres
		mov bl, mres //mres
		mov dres, bl
		mov mres, cl
		
	}
	printf("%d\n", mres);      //division result
	printf("%d\n", dres);     //multiply result

}