title addition of 3 numbers in byte format
include \masm32\include\masm32rt.inc
.data
a1 db 0ffh,0 ;
i dd 1,0 ;
b dd 2,0 ;
c1 dd 0,0 ;
d dd 0,0 ;
res dd 0,0 ;
titl db "Transforming of first lab to MASM: ",0; simplified window name
st1 dq 1 dup(0),0 ; message output buffer
ifmt db "Displaying variants to calculate formula: ",0ah,
"i = 1", 10, "b = 2",10, "c = b - i",10,"d = c + b",10,"Result: ",10,
"res = d + d + c = %x",0ah,0ah
.code
entry_point proc
xor eax,eax ; zeroing - so as not to see garbage

	    mov eax, [i]
		mov ebx, [b]
		xor ecx, ecx
		xor edx, edx
		 
		sub ebx, eax      ; c = b - i = 1
		mov ecx, ebx
		mov [c1], ecx

		add ecx, [b]       ;d = c + b = 3
		mov edx, ecx

		add edx, edx     ;res = d + d + c = 7
		add edx, [c1]
		;mov [res], edx

movzx edx,dx ; extension (can be omitted if there is no garbage)
invoke wsprintf, ADDR st1, ADDR ifmt,edx;
invoke MessageBox,0,addr st1,addr titl, MB_OK
invoke ExitProcess,0
entry_point endp
end