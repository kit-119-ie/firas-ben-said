NULL              EQU 0                         ; Constants
STD_OUTPUT_HANDLE EQU -11


global Start

extern _ExitProcess@4
extern printf         ;from msvcrt

section .bss
name:   resb 100

section .data

struc university 
 
  .student_name:      resq    1 
  .scholarship:      resq    1 
  .grade:      resq    1 
  .age:      resq    1 
 
endstruc

str1:
  istruc university
   at university.student_name    
     dd "firas"
   at university.scholarship
     dd 1700
   at university.grade 
     dd 4
   at university.age
     dd 23
  iend

str2:
  istruc university
   at university.student_name    
     dd "salah"
   at university.scholarship
     dd 1000
   at university.grade 
     dd 5
   at university.age
     dd 20
  iend
str3:
  istruc university
   at university.student_name    
     dd "ronaldo"
   at university.scholarship
     dd 1300
   at university.grade 
     dd 4
   at university.age
     dd 25
  iend
str4:
  istruc university
   at university.student_name    
     dd "messi"
   at university.scholarship
     dd 3000
   at university.grade 
     dd 3
   at university.age
     dd 28
  iend
  
titl1 dq "The average age of the students: "

temp db "%d",0ah,0

need_stop db 0

section .text
Start:
push titl1
call printf

mov eax, [str1 +  university.age]        ;eax holds the sum of ages
add eax, [str2 +  university.age]
add eax, [str3 +  university.age]
add eax, [str4 +  university.age]

mov ebx, 4
div ebx

push eax
push temp
call    printf

		
 push  NULL
 call  _ExitProcess@4